// Set up
var express = require('express');
var app = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var cors = require('cors');
const url ="mongodb://localhost:27017/attendance-app"
// Configuration
mongoose.connect(url, {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({ 'extended': 'true' }));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Modelss

var Attendance = mongoose.model('Attendance', {
  id_staff: String,
  Status: String,
  checkin_date: Date,
  checkin_time: Number,
  checkout_time: Number,
  
});

// Routes

// Get attendance
app.get('/api/attendances/:sid', function (req, res) {

  console.log("fetching attendannces");
  console.log(req.params.sid);
  Attendance.find({id_staff: req.params.sid}).sort('-checkin_date').exec()
    .then(docs => {
      if (docs.length > 0) {
        res.json({docs})
      }
      else {
        res.json({msg: "No attendance found."})
      }
    })

  // use mongoose to get all reviews in the database
  // Attendance.find({id_staff: req.params.key}, function (err, attendances) {

  //   // if there is an error retrieving, send the error. nothing after res.send(err) will execute
  //   if (err)
  //     res.send(err)
  //   console.log(attendances);
  //   res.json(attendances); // return all reviews in JSON format
  // });
});

// create review and send back all reviews after creation
app.post('/api/attendances/in', function (req, res) {

  console.log("creating attendance");
  console.log((new Date).toDateString());
  Attendance.find({id_staff: req.body.id_staff, checkin_date: (new Date).toDateString()}).exec()
    .then(docs => {
      if (docs.length > 0) {
        return res.json({msg: "Already registered for today."});
      } else {
        Attendance.create({
          id_staff: req.body.id_staff,
          // Status: req.body.Status,
          checkin_date: (new Date).toDateString(),
          checkin_time: Date.now(),
          checkout_time: null,
        }, function (err, attendance) {
          if (err)
             return res.json({msg: err});
          res.json({attendance}); 
          // get and return all the reviews after you create another
          // Attendance.find(function (err, attendances) {
          //   if (err)
          //     return res.json({msg:err})
          //   res.json(attendances);
          // });
        });
      }
    });
  // create a review, information comes from request from Ionic
});
app.post('/api/attendances/out', function (req, res) {

  console.log("checkout attendance");
  console.log((new Date).toDateString());
  Attendance.findOne({id_staff: req.body.id_staff, checkin_date: (new Date).toDateString()}).exec()
    .then(doc => {
      if (doc) {
        if (doc.checkout_time !== null) {
          return res.json({msg: "Already checked out"});
        }
        doc.checkout_time = Date.now();
        doc.save();
        res.json({attendance: doc});
      } else{
        res.json({msg: "Not yet checkin"});
      }
    })
  // Attendance.find({id_staff: req.body.id_staff, checkin_date: (new Date).toDateString()}).exec()
  //   .then(docs => {
  //     if (docs.length > 0) {
  //       return res.json({msg: "Already registered for today."});
  //     } else {
  //       Attendance.create({
  //         id_staff: req.body.id_staff,
  //         // Status: req.body.Status,
  //         checkin_date: (new Date).toDateString(),
  //         checkin_time: Date.now(),
  //         checkout_date: null,
  //         checkout_time: null,
  //       }, function (err, attendance) {
  //         if (err)
  //            return res.json({msg: err});
  //         res.json({attendance}); 
  //         // get and return all the reviews after you create another
  //         // Attendance.find(function (err, attendances) {
  //         //   if (err)
  //         //     return res.json({msg:err})
  //         //   res.json(attendances);
  //         // });
  //       });
  //     }
  //   });
  // create a review, information comes from request from Ionic
});
// delete a review
app.delete('/api/attendances/:attendance_id', function (req, res) {
  Attendance.remove({
    _id: req.params.attendance_id
  }, function (err, attendance) {

  });
});


// listen (start app with node server.js) ======================================
app.listen(8080, () => console.log('app listening on port 8080'));