import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  value: string = '';

  constructor(public http: HttpClient ,private nav : NavController) {}

  submit() {
    if (this.value === '') return
    this.http.post('http://localhost:8080/api/attendances/in', { id_staff: this.value }).subscribe(data => {
      if (data['msg'])
        alert(data['msg']);
      else {
        console.log(data);
        alert("Checkin in for " + this.value + "\n" + "Date: " + new Date(data['attendance'].checkin_time).toDateString() + "\nTime: " + new Date(data['attendance'].checkin_time).toLocaleTimeString());
        this.nav.navigateRoot('/'); 
      }
    })
  }

}
