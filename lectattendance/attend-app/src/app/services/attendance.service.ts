import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  data: any;

  constructor(public http: HttpClient) {
    this.data = null;
  }

  getAttendances() {

    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {

      this.http.get('http://localhost:27017/api/attendances')
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });

  }

  createAttendance(attendance) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return new Promise(resolve => {
      this.http.post('http://localhost:27017/api/attendances', attendance, { headers: headers })
        .subscribe(data => {
          this.data = data;
          resolve(data);
        });
    })
  }

  deleteAttendance(id) {
    console.log(id);
    this.http.delete('http://localhost:27017/api/attendances/' + id).subscribe((res) => {
      console.log(res);
    });

  }
}
