import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckattendancePage } from './checkattendance.page';

describe('CheckattendancePage', () => {
  let component: CheckattendancePage;
  let fixture: ComponentFixture<CheckattendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckattendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckattendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
