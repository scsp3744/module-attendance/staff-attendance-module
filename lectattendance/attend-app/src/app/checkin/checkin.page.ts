import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.page.html',
  styleUrls: ['./checkin.page.scss'],
})
export class CheckinPage implements OnInit {

  ID = null;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.ID = this.activatedRoute.snapshot.paramMap.get('id');
  }

}
