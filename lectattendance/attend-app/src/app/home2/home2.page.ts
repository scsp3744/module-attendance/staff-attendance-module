import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home2',
  templateUrl: 'home2.page.html',
  styleUrls: ['home2.page.scss'],
})
export class Home2Page {

  value: string = '';

  constructor(public http: HttpClient ,private nav : NavController) {}

  submit() {
    if (this.value === '') return
    this.http.post('http://localhost:8080/api/attendances/out', { id_staff: this.value }).subscribe(data => {
      if (data['msg'])
        alert(data['msg']);
      else {
        console.log(data);
        alert("Checkout in for " + this.value + "\n" + "Date: " + new Date(data['attendance'].checkout_time).toDateString() + "\nTime: " + new Date(data['attendance'].checkout_time).toLocaleTimeString());

        this.nav.navigateRoot('/');
      }
    })
  }

  

}
